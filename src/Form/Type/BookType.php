<?php
namespace App\Form\Type;

use App\Entity\{Book, Author};

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('author', EntityType::class, [
                'class' => Author::class,
                'label' => 'Autor',
                'choice_label' => 'last_name',
                'data' => $options['author'] ?? null,
                'invalid_message' => 'Wybierz autora z listy lub dodaj nowego.'
            ])
            ->add('title', TextType::class, [
                'label' => 'Tytuł',
                'empty_data' => '',
                'invalid_message' => 'Podaj tytuł książki.'
            ])
            ->add('year', ChoiceType::class, [
                'label' => 'Rok wydania',
                'choices' => $this->prepareYearsSinceTo(1800),
                'empty_data' => ' ',
                'invalid_message' => 'Podaj rok wydania książki.'
            ])
            ->add('isbn', TextType::class, [
                'label' => 'ISBN',
                'empty_data' => '',
                'invalid_message' => 'Podaj numer ISBN.'
            ])
            ->add('cover', FileType::class, [
                'label' => 'Okładka (png, jpg, jpeg)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5120k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'Dozwolony format pliku to PNG, JPG i JPEG.',
                    ])
                ],
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'author' => null
        ]);
    }

    private function prepareYearsSinceTo($min, $max = null)
    {
        $years = range($min, ($max ?: date('Y')) );

        $years = array_combine($years, $years);

        return array_reverse($years, true);
    }
}
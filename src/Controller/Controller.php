<?php
namespace App\Controller;

use Doctrine\ORM\Query;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Controller extends AbstractController{

    public function __construct(EntityManagerInterface $entityManager){

        $this->entityManager = $entityManager;
    }

    protected function repository( $repositoryClass ){

        return $this->getDoctrine()->getRepository( $repositoryClass );
    }

    protected function paginate(Query $query, $page = 1, $perPage = 10){

        $paginator = new Paginator($query);

        $paginator->getQuery()
            ->setFirstResult($perPage * ($page - 1))
            ->setMaxResults($perPage);

        $paginator->currentPage = $page;
        $paginator->lastPage = ceil($paginator->count() / $perPage);

        return $paginator;
    }
}
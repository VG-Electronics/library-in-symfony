<?php
namespace App\Controller;

use App\Entity\{Book, Author};
use App\Form\Type\BookType;

use App\Service\FileManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BooksController extends Controller{

    /** @Route("/books", name="books.index") */
    public function index(){
        
        $authors = $this->repository(Author::class)->findAll();

        return $this->render('books/index.html.twig', compact('authors'));
    }

    /** @Route("/books/search", name="books.search") */
    public function search(Request $request){

        $filters = $request->request->all();

        $booksQuery = $this->repository(Book::class)->search($filters, true);

        $books = $this->paginate($booksQuery, $filters['page'], $filters['per_page']);

        return $this->render('/books/partial/booksList.html.twig', compact('books'));
    }

    /** @Route("/books/{id}", name="books.show", requirements={"id":"\d+"}) */
    public function show(Book $book){

        return $this->render('books/show.html.twig', compact('book'));
    }
    
    /** @Route("/books/create/{author}", name="books.create") */
    public function create(Author $author = null, Request $request){
        
        $form = $this->createForm(BookType::class, new Book, ['author' => $author]);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
            return $this->saveAndRedirect($form);
        
        return $this->render('books/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /** @Route("/books/update/{id}", name="books.update", requirements={"id":"\d+"}) */
    public function update(Book $book, Request $request){

        $form = $this->createForm(BookType::class, $book, ['author' => $book->getAuthor()]);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
            return $this->saveAndRedirect($form);
        
        return $this->render('books/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /** @Route("/books/delete/{id}", name="books.delete", methods="DELETE", requirements={"id": "\d+"}) */
    public function delete(Book $book){
        
        $this->repository(Book::class)->delete($book);

        return $this->redirectToRoute('books.index');
    }

    /** @Route("/books/delete-cover/{id}", name="books.delete-cover", methods="GET", requirements={"id": "\d+"}) */
    public function deleteCover(Book $book){
        
        $this->repository(Book::class)->deleteCover($book);

        return $this->redirectToRoute('books.show', ['id' => $book->getId()]);
    }

    private function saveAndRedirect($form){

        $book = $this->repository(Book::class)->saveByForm($form);

        return $this->redirectToRoute('books.show', ['id' => $book->getId()]);
    }
}
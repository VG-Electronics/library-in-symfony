<?php
namespace App\Controller;

use App\Entity\Author;
use App\Form\Type\AuthorType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorsController extends Controller{

    /** @Route("/authors", name="authors.index") */
    public function index(){
        
        $authors = $this->repository(Author::class)->findAll();    

        return $this->render('authors/index.html.twig', compact('authors'));
    }

    /** @Route("/authors/{id}", name="authors.show", requirements={"id":"\d+"}) */
    public function show(Author $author){

        return $this->render('authors/show.html.twig', compact('author'));
    }
    
    /** @Route("/authors/create", name="authors.create") */
    public function create(Request $request){

        $form = $this->createForm(AuthorType::class, new Author);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
            return $this->saveAndRedirect( $form->getData() );
        
        return $this->render('authors/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /** @Route("/authors/update/{id}", name="authors.update", requirements={"id":"\d+"}) */
    public function update(Author $author, Request $request){

        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
            return $this->saveAndRedirect($author);
        
        return $this->render('authors/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /** @Route("/authors/delete/{id}", name="authors.delete", methods="DELETE", requirements={"id": "\d+"}) */
    public function delete(Author $author){
        
        $this->repository(Author::class)->delete($author);

        return $this->redirectToRoute('authors.index');
    }

    private function saveAndRedirect(Author $author){

        $this->repository(Author::class)->save($author);

        return $this->redirectToRoute('authors.show', ['id' => $author->getId()]);
    }
}
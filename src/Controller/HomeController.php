<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller{
    
    /** @Route("/", name="home.index") */
    public function index(){
        
        return $this->render('home.html.twig');
    }
}
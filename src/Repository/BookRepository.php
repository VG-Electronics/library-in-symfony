<?php

namespace App\Repository;

use App\Entity\Book;

use App\Service\FileManager;

use App\Repository\Search\QuerySearcher;

use Symfony\Component\Form\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class BookRepository extends ServiceEntityRepository
{
    const CoverPath = 'covers/';

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager, FileManager $fileManager)
    {
        $this->entityManager = $entityManager;
        $this->fileManager = $fileManager;
        $this->searcher = new QuerySearcher;

        $this->searcher->setDirectSearchFields(['id', 'author']);
        $this->searcher->setPatternSearchFields(['title']);

        parent::__construct($registry, Book::class);
    }

    public function search(array $data, $returnQuery = false){

        $queryBuilder = $this->createQueryBuilder('b');

        $query = $this->searcher->queryFilters($queryBuilder, $data)->getQuery();

        return $returnQuery ? $query : $query->execute();
    }
    
    public function saveByForm(Form $form){

        $book = $form->getData();

        if ($coverFile = $form->get('cover')->getData()){
            
            $this->fileManager->removeFileIfExists($book->getCoverFilePath(false));
            
            $coverFileName = $this->fileManager->upload($coverFile, self::CoverPath);
            $book->setCoverFileName($coverFileName);
        }

        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $book;
    }

    public function deleteCover(Book $book){

        $this->fileManager->removeFileIfExists($book->getCoverFilePath(false));

        $book->setCoverFileName('');

        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $book;
    }

    public function delete(Book $book){

        $this->entityManager->remove($book);
        $this->entityManager->flush();
    }
}

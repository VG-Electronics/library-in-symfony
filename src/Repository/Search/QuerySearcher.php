<?php
namespace App\Repository\Search;

use Doctrine\ORM\QueryBuilder;

class QuerySearcher{

    private $sortingOptions = [
        'latest' => ['id', 'DESC'],
        'oldest' => ['id', 'ASC'],
        'published_latest' => ['year', 'DESC'],
        'published_oldest' => ['year', 'ASC'],
    ];

    private $query;
    private $alias;
    private $directSearchFields = [];
    private $patternSearchFields = [];

    public function queryFilters(QueryBuilder $query, array $filters){

        $this->query = $query;
        $this->alias = current($query->getDQLPart('from'))->getAlias();

        $sort = $filters['sort'] ?? null;

        foreach( $filters as $field => $value){
            if( !$value )
                continue;

            if( $this->isDirectSearch($field) )
                $this->whereIs($field, $value);
            elseif( $this->isPatternSearch($field) )
                $this->whereLike($field, $value);
        }

        if( $sort && isset($this->sortingOptions[$sort]) )
            $this->sortBy($sort);

        return $this->query;
    }

    public function setDirectSearchFields(array $fields){
        $this->directSearchFields = $fields;
    }
    
    public function setPatternSearchFields(array $fields){
        $this->patternSearchFields = $fields;
    }

    private function isDirectSearch(string $field){
        return in_array($field, $this->directSearchFields);
    }

    private function isPatternSearch(string $field){
        return in_array($field, $this->patternSearchFields);
    }

    private function whereIs(string $field, $value){
        $this->query = $this->query->andWhere( $this->field($field).' = :'.$field )->setParameter($field, $value);
    }

    private function whereLike(string $field, $value){
        $this->query = $this->query->andWhere( $this->field($field).' LIKE :'.$field )->setParameter($field, $value);
    }

    private function sortBy(string $sortingOption){
        list($key, $order) = $this->sortingOptions[$sortingOption];

        $this->query = $this->query->orderBy( $this->field($key), $order);
    }

    private function field(string $field){
        return $this->alias.'.'.$field;
    }
}
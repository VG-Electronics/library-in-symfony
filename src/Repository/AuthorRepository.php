<?php

namespace App\Repository;

use App\Entity\Author;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct($registry, Author::class);
    }

    public function save(Author $author){

        $this->entityManager->persist($author);
        $this->entityManager->flush();
    }

    public function delete(Author $author){

        foreach($author->getBooks() as $book)
            $this->entityManager->remove($book);
    
        $this->entityManager->remove($author);
        $this->entityManager->flush();
    }
}

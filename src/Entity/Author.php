<?php

namespace App\Entity;

use App\Formatter\NameFormatter;

use App\Repository\AuthorRepository;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\OneToMany(targetEntity="App\Entity\Book", mappedBy="author") */
    private $books;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Podaj imię.")
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Podaj nazwisko.")
     */
    private $last_name;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /** @return Collection|Book[] */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    public function getFirstName(): ?string
    {
        return NameFormatter::formatName($this->first_name);
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return NameFormatter::formatName($this->last_name);
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }
}

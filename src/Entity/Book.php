<?php

namespace App\Entity;

use App\Repository\BookRepository;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="books")
     * @Assert\NotNull(message="Wybierz autora książki lub dodaj nowego.")
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Podaj tytuł książki.")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=4)
     * @Assert\Range(
     *      min = 1800,
     *      max = "today",
     *      notInRangeMessage = "Wybierz rok pomiędzy {{ min }} a {{ max }}.",
     * )
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Podaj numer ISBN.")
     */
    private $isbn;

    /**
     * @ORM\Column(type="string")
     */
    private $coverFileName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function hasImage(): bool
    {
        return $this->getCoverFileName() ? true : false;
    }

    public function getCoverFilePath($default = true): ?string
    {
        return $this->getCoverFileName() ? 'uploads/covers/'.$this->getCoverFileName() : ($default ? 'images/defaultCover.png' : '');
    }

    public function getCoverFileName(): ?string
    {
        return $this->coverFileName;
    }

    public function setCoverFileName(string $coverFileName): self
    {
        $this->coverFileName = $coverFileName;

        return $this;
    }
}

<?php
namespace App\Formatter;

class NameFormatter{

    public static function formatName( $name ){
        return ucfirst( mb_strtolower($name) );
    }
}
<?php
namespace App\Service;

use Gedmo\Sluggable\Util\Urlizer;

use League\Flysystem\FilesystemInterface;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileManager
{
    public function __construct(FilesystemInterface $publicUploadsFilesystem)
    {
        $this->filesystem = $publicUploadsFilesystem;
    }

    public function upload(File $file, string $subPath = '')
    {
        $newFileName = $this->createNewNameForFile($file);

        $file = $this->filesystem->write( $subPath.$newFileName, file_get_contents($file->getPathname()) );

        return $newFileName;
    }

    public function removeFileIfExists(string $filePath){
        if( file_exists($filePath) )
            unlink($filePath);
    }

    private function createNewNameForFile(File $file){

        $originalName = ( $file instanceof UploadedFile ? $file->getClientOriginalName() : $file->getFilename() );

        $safeFileName = Urlizer::urlize(pathinfo($originalName, PATHINFO_FILENAME));

        return $safeFileName.'-'.uniqid().'.'.$file->guessExtension();
    }
}
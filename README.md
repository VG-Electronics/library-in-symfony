<h1>Library project in Symfony</h1>
<p>First demo project made in Symfony.</p>

<h2>Installation</h2>
1. Clone the repository<br>
2. Make <b>.env</b> based on <b>example.env</b><br>
3. Run "composer install"<br>
4. Run "php bin/console doctrine:database:create"<br>
5. Run "php bin/console doctrine:migrations:migrate"<br>

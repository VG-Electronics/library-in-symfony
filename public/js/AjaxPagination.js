/* Service of partial pagination

    1. Include partial "ajaxPagination"
    2. Include current script
    3. Set AJAX callbackFunction (use Pagination.currentPage to set page in request)
*/

let Pagination = {
    currentPage: 1,

    callbackFunction: function(){
        console.error('No callback function specified')
    },

    isNumber: function(keyCode){
        return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105);
	},
	isEnter: function(keyCode){ 
        return keyCode == 13 
    }
}

let PaginationEffects = {
    loadingElem: null,

    loadOpacityEffect: function(turnOn){
        this.loadingElem.fadeTo(500, turnOn ? 0.5 : 1);
    },

    showDataWithEffect: function( displayData ){
        let object = this

        $('html, body').animate({ scrollTop: this.loadingElem.offset().top -200}, 500, function(){
            object.loadingElem.html( displayData )
            object.loadOpacityEffect( false )
        })
    }
}

$(document).on('keyup', '#currentPage', function(e){
    if( !Pagination.isNumber(e.keyCode) && !Pagination.isEnter(e.keyCode) )
        return false;

    let selectedPage = $('#currentPage');
    
    if( selectedPage.val() > $('#maxPage').data('page') )
        selectedPage.val( $('#maxPage').data('page') );

    if( selectedPage.val() < 1 )
        selectedPage.val( 1 );

    Pagination.currentPage = selectedPage.val();
    Pagination.callbackFunction();
})

$(document).on('click', '#pageIncrease, #pageDecrease', function(){
    
    $(this).attr('id') == 'pageIncrease' ? Pagination.currentPage++ : Pagination.currentPage--;

    var e = $.Event('keyup');
    e.keyCode = 49; // 1
    $('#currentPage').val(Pagination.currentPage).trigger(e);
})  